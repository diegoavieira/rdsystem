import { createStyles } from '@material-ui/core';

const RdsFrameStyles = () =>
  createStyles({
    root: {
      width: '100%',
      height: 400,
      border: 'none'
    }
  });

export default RdsFrameStyles;
