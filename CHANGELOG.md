# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [0.1.7](https://gitlab.com/diegoavieira/rdsystem/compare/v0.1.6...v0.1.7) (2021-11-18)


### Bug Fixes

* adjustments ([fc1058c](https://gitlab.com/diegoavieira/rdsystem/commit/fc1058cac4631651f12f284cc31f305455a08a5e))






## [0.1.6](https://gitlab.com/diegoavieira/rdsystem/compare/v0.1.5...v0.1.6) (2021-11-17)


### Bug Fixes

* adjustments ([5df55c0](https://gitlab.com/diegoavieira/rdsystem/commit/5df55c00d14461edabf2201d46bd9a59afa852ed))





## [0.1.5](https://gitlab.com/diegoavieira/rdsystem/compare/v0.1.4...v0.1.5) (2021-11-17)


### Bug Fixes

* adjustments ([99c4fc2](https://gitlab.com/diegoavieira/rdsystem/commit/99c4fc2803f96470b761e1815be7caa7b350bbcf))
* adjustments ([e0f0e0d](https://gitlab.com/diegoavieira/rdsystem/commit/e0f0e0d978013d76c14c022de3aedf45ee475e4f))





## [0.1.4](https://gitlab.com/diegoavieira/rdsystem/compare/v0.1.3...v0.1.4) (2021-11-17)


### Bug Fixes

* adjustments ([482d6d5](https://gitlab.com/diegoavieira/rdsystem/commit/482d6d54965c3e743c656d2f63c06b61600d39c8))





## [0.1.3](https://gitlab.com/diegoavieira/rdsystem/compare/v0.1.2...v0.1.3) (2021-11-17)


### Bug Fixes

* adjustments ([813d825](https://gitlab.com/diegoavieira/rdsystem/commit/813d8252c39cfdd42c62b006dd7fd97a3b359828))





## [0.1.2](https://gitlab.com/diegoavieira/rdsystem/compare/v0.1.1...v0.1.2) (2021-11-17)


### Bug Fixes

* adjustments ([7ac423d](https://gitlab.com/diegoavieira/rdsystem/commit/7ac423d5e3ddcf8ce046a0eea45f4e32d5acd13d))





## [0.1.1](https://gitlab.com/diegoavieira/rdsystem/compare/v0.1.0...v0.1.1) (2021-11-17)


### Bug Fixes

* adjustments ([09bab8f](https://gitlab.com/diegoavieira/rdsystem/commit/09bab8f807769cfb6cab228c1f1a662573cacb58))





# [0.1.0](https://gitlab.com/diegoavieira/rdsystem/compare/v0.0.35...v0.1.0) (2021-11-17)


### Bug Fixes

* adjustments ([47ecd8c](https://gitlab.com/diegoavieira/rdsystem/commit/47ecd8c4aabda5252c5601ee473497985f58a2bb))


### Features

* adjustemnts ([03020b5](https://gitlab.com/diegoavieira/rdsystem/commit/03020b5138af515065712bae23770425b01b561f))
* adjustents ([4a7c93c](https://gitlab.com/diegoavieira/rdsystem/commit/4a7c93c41eec8e3c326bfc38c74766b69e6ee00f))
* adjustents ([6fcc871](https://gitlab.com/diegoavieira/rdsystem/commit/6fcc8717c2503593929bc286865f58106cda15b6))
* adjustents ([a863904](https://gitlab.com/diegoavieira/rdsystem/commit/a8639043685cea90266640ad96820abba5f92262))
* adjustments ([5a40f05](https://gitlab.com/diegoavieira/rdsystem/commit/5a40f053610fe5fe6d38d45bd90c9a51c544a58a))
* adjustments ([d4fe4ae](https://gitlab.com/diegoavieira/rdsystem/commit/d4fe4ae192583d5aed86bb27d398183837e1451f))
* adjustments ([13102b9](https://gitlab.com/diegoavieira/rdsystem/commit/13102b9ddd9243a569d184bbf5e05dddd17206f3))
* adjustments ([6bb5646](https://gitlab.com/diegoavieira/rdsystem/commit/6bb56460ae410834cdb768aec7f5402b79f28733))
* adjustments ([a829c75](https://gitlab.com/diegoavieira/rdsystem/commit/a829c75a2ca65bac8641072a9d1bd5ed81981d24))
* adjustments ([5329a78](https://gitlab.com/diegoavieira/rdsystem/commit/5329a786e2ec53d4e4be05318d172ceb6cde9eb0))
* adjustments ([a695ce0](https://gitlab.com/diegoavieira/rdsystem/commit/a695ce03eb7c91c9dc303c49d5aa72b51f7d67de))
* adjustments ([cc4f18c](https://gitlab.com/diegoavieira/rdsystem/commit/cc4f18cd1b5b34ad53ddc5c08aea409304cd32be))
* adjustments ([a7deef3](https://gitlab.com/diegoavieira/rdsystem/commit/a7deef3e2b845a8f8582e21d254090d40a79ec37))
* adjustments ([3d9a2ac](https://gitlab.com/diegoavieira/rdsystem/commit/3d9a2acacd4aafefd0e5c1bb5ecc34fcbb487998))
* adjustments ([352876e](https://gitlab.com/diegoavieira/rdsystem/commit/352876e13643c042033b4a81970ff70eed98b5f0))
* adjustments ([ddd142f](https://gitlab.com/diegoavieira/rdsystem/commit/ddd142ffde3a95153c064c0727bc4b6254b34de2))
* adjustments ([926341c](https://gitlab.com/diegoavieira/rdsystem/commit/926341cce96e876aed101a49bb49f80c86caff6c))
* adjustments ([317c63b](https://gitlab.com/diegoavieira/rdsystem/commit/317c63b720d81ced55737a5983fc885045f924da))
* adjustments ([8ac9552](https://gitlab.com/diegoavieira/rdsystem/commit/8ac9552eb5c480b5fe256e3bfbd843c308e5d1ac))
* adjustments ([0fc8668](https://gitlab.com/diegoavieira/rdsystem/commit/0fc8668dfb95612c345b510eb0249dec9fd30f28))
* adjustments ([97a7cb8](https://gitlab.com/diegoavieira/rdsystem/commit/97a7cb81cdc683ca5c55d2fb56a88d1ea38b6f9c))
* adjustments ([949b7f2](https://gitlab.com/diegoavieira/rdsystem/commit/949b7f2a0c3f71e1eedbbd3000b7ef8640a7b21c))
* adjustments ([2540acf](https://gitlab.com/diegoavieira/rdsystem/commit/2540acf2184b20a1c25020458f765c882f2aa058))
* adjustments ([a18f625](https://gitlab.com/diegoavieira/rdsystem/commit/a18f625399e129b592fd59e30159eb31efd412cf))
* adjustments ([a840d2b](https://gitlab.com/diegoavieira/rdsystem/commit/a840d2b77a55a37aae4d796dff07973bd084c0a1))
* adjustments ([add7659](https://gitlab.com/diegoavieira/rdsystem/commit/add765994cff632b1c74f017b92d9f141598f57b))
* adjustments ([808e9f1](https://gitlab.com/diegoavieira/rdsystem/commit/808e9f1fcdcd40c0c058f567361205ef3b85acb6))
* adjustments ([87b3e9a](https://gitlab.com/diegoavieira/rdsystem/commit/87b3e9a693c9a79b733f869551b9366268771850))
* adjustments ([84686b6](https://gitlab.com/diegoavieira/rdsystem/commit/84686b68ccad6882d2f6fae012a3f0e478f60273))
* adjustments ([d391f49](https://gitlab.com/diegoavieira/rdsystem/commit/d391f4983666250495f54585ea91536d91c74434))
* adjustments ([e5dbb82](https://gitlab.com/diegoavieira/rdsystem/commit/e5dbb829b11bc4894851e748f4e7f4c11bc478ff))
* adjustments ([d8f03b0](https://gitlab.com/diegoavieira/rdsystem/commit/d8f03b01defe3ebb8804bd07d5816ec1d14b1860))
* adjustments ([2a0e73f](https://gitlab.com/diegoavieira/rdsystem/commit/2a0e73f33cc11c47f7f6d9174777e5ef57ddb837))
* adjustments ([dcd2ff2](https://gitlab.com/diegoavieira/rdsystem/commit/dcd2ff26bffbda932cca6f5d7838a23246971310))
* adjustments ([5d523a3](https://gitlab.com/diegoavieira/rdsystem/commit/5d523a30deb363d54d22e0148c9b53f9476ecc72))
* adjustments ([7510b0b](https://gitlab.com/diegoavieira/rdsystem/commit/7510b0b94a6a037bf0dd84067fbfc933da4ad63c))
* adjustments ([98cbfd8](https://gitlab.com/diegoavieira/rdsystem/commit/98cbfd8fc9b8cd6761b4ae4a867ba6e1e3105dc1))
* adjustments ([c25d8f2](https://gitlab.com/diegoavieira/rdsystem/commit/c25d8f2f82c35dd88fbdae6025dde06d9a38e68a))
* adjustments ([9475cdd](https://gitlab.com/diegoavieira/rdsystem/commit/9475cdd5d389ff3e559199f1f29d2b12766652e4))
* adjustments ([1f8f3f5](https://gitlab.com/diegoavieira/rdsystem/commit/1f8f3f51379c2fdf1d39fcad5fb6b49c88cf3079))
* adjustments ([32106fa](https://gitlab.com/diegoavieira/rdsystem/commit/32106fa980e699e7bf1492048bccd3da10a51890))
* adjustments ([69fb905](https://gitlab.com/diegoavieira/rdsystem/commit/69fb90565075d4d862d625517ca2f9580b62db77))
* adjustments ([09a23a9](https://gitlab.com/diegoavieira/rdsystem/commit/09a23a942be164b0e8b2056b46783e268e574b91))
* adjustments ([f132031](https://gitlab.com/diegoavieira/rdsystem/commit/f13203121ee01bc76628cf6a38d321a71b8753e8))
* adjustments ([767da2a](https://gitlab.com/diegoavieira/rdsystem/commit/767da2a0d8a8bb01699233560ecbeea0fd4b474d))
* adjustments ([23a15c9](https://gitlab.com/diegoavieira/rdsystem/commit/23a15c9fd51b5955234f7fa824e11c55490f76ec))
* adjustments ([782df67](https://gitlab.com/diegoavieira/rdsystem/commit/782df67b30bf15a4b1d825a626bc8986374eb53d))
* adjustments ([4e6c08d](https://gitlab.com/diegoavieira/rdsystem/commit/4e6c08dd30b559c5d06536c2ce1f6e1a512adb98))
* adjustments ([af8c066](https://gitlab.com/diegoavieira/rdsystem/commit/af8c066629ec0c6aef92fa64261153a650a2f38b))
* adjustments ([9b249ba](https://gitlab.com/diegoavieira/rdsystem/commit/9b249ba94048b20588e1149142bab00fe61eafca))
* adjuststments ([36bfd78](https://gitlab.com/diegoavieira/rdsystem/commit/36bfd78ddfe1a523822217e4b220e6617f6284c7))
* adjuststments ([5461554](https://gitlab.com/diegoavieira/rdsystem/commit/5461554c4279823daf8ee0599d198c855a884457))






## [0.0.35](https://gitlab.com/diegoavieira/rdsystem/compare/v0.0.34...v0.0.35) (2021-09-23)


### Bug Fixes

* adjustmemts ([6d34edb](https://gitlab.com/diegoavieira/rdsystem/commit/6d34edb840db42bf194188943e8add6c0db66277))
* adjustments ([84253bc](https://gitlab.com/diegoavieira/rdsystem/commit/84253bc738b72f6f24a6b8e8c5c14a1b1395ea36))
* adjustments ([ca1bd26](https://gitlab.com/diegoavieira/rdsystem/commit/ca1bd2630768d29fa9aacb5d35009a27e7c1e8df))
* adjustments ([935ee0b](https://gitlab.com/diegoavieira/rdsystem/commit/935ee0b94fa9f39ed7b0735eec75add672ab7be2))
* adjustments ([d437c78](https://gitlab.com/diegoavieira/rdsystem/commit/d437c7864df961f3f5fff542b2de3cf151fd2684))
* adjustments ([3ce0380](https://gitlab.com/diegoavieira/rdsystem/commit/3ce0380691cbf7a426bcdba29c9ecd3f7cd6910d))
* adjustments ([e8a8364](https://gitlab.com/diegoavieira/rdsystem/commit/e8a83641d73ef6aeb7891c074cae22a7daec54e3))
* adjustments ([b40d8bf](https://gitlab.com/diegoavieira/rdsystem/commit/b40d8bf8774a51c36f8eb196fe46c98ba3bc3151))
* adjustments ([34cd7a9](https://gitlab.com/diegoavieira/rdsystem/commit/34cd7a90a565d0bdfd44d3d9069fbb97ca101e40))






## [0.0.34](https://gitlab.com/diegoavieira/rdsystem/compare/v0.0.33...v0.0.34) (2021-09-21)


### Bug Fixes

* adjustments ([5ac882c](https://gitlab.com/diegoavieira/rdsystem/commit/5ac882ccd1e6ec4d4a59bc289565f0ef3f6b6c23))






## [0.0.33](https://gitlab.com/diegoavieira/rdsystem/compare/v0.0.32...v0.0.33) (2021-09-21)


### Bug Fixes

* adjustments ([554fb69](https://gitlab.com/diegoavieira/rdsystem/commit/554fb69b3ef3067294c8640cf42a3401edda0ed4))





## [0.0.32](https://gitlab.com/diegoavieira/rdsystem/compare/v0.0.31...v0.0.32) (2021-09-21)


### Bug Fixes

* adjustments ([78f43b4](https://gitlab.com/diegoavieira/rdsystem/commit/78f43b41c709ecc8804bef921be69f92d0591e10))





## [0.0.31](https://gitlab.com/diegoavieira/rdsystem/compare/v0.0.30...v0.0.31) (2021-09-21)


### Bug Fixes

* adjustments ([4d1e16b](https://gitlab.com/diegoavieira/rdsystem/commit/4d1e16b3ac0e0a65250fe5da9f183a75d2763bfc))





## [0.0.30](https://gitlab.com/diegoavieira/rdsystem/compare/v0.0.29...v0.0.30) (2021-09-21)


### Bug Fixes

* adjustments ([cb23f02](https://gitlab.com/diegoavieira/rdsystem/commit/cb23f0296bb942d9a47742c708d28f6bc5c30036))





## [0.0.29](https://gitlab.com/diegoavieira/rdsystem/compare/v0.0.28...v0.0.29) (2021-09-21)


### Bug Fixes

* adjustments ([9c4ce0f](https://gitlab.com/diegoavieira/rdsystem/commit/9c4ce0f9ec68808b1f28a702e212b6ff65e0d2e6))





## [0.0.28](https://gitlab.com/diegoavieira/rdsystem/compare/v0.0.27...v0.0.28) (2021-09-21)


### Bug Fixes

* adjustments ([f6afa4e](https://gitlab.com/diegoavieira/rdsystem/commit/f6afa4e93dfe63588bca163af33d0e31c39bd3fa))
* adjustments ([acc7216](https://gitlab.com/diegoavieira/rdsystem/commit/acc7216b124e911dc716fe2eb512718d0980382a))
* adjustments ([4eafb8e](https://gitlab.com/diegoavieira/rdsystem/commit/4eafb8ee07f0ceba99010913cd019a1d3844b7bc))






## [0.0.27](https://gitlab.com/diegoavieira/rdsystem/compare/v0.0.26...v0.0.27) (2021-09-20)


### Bug Fixes

* adjustments ([2806682](https://gitlab.com/diegoavieira/rdsystem/commit/280668224abc302ac124caabccc94e76e8c167e0))





## [0.0.26](https://gitlab.com/diegoavieira/rdsystem/compare/v0.0.25...v0.0.26) (2021-09-20)


### Bug Fixes

* adjustments ([c96a425](https://gitlab.com/diegoavieira/rdsystem/commit/c96a425100285825667321e7bbecf481f57da0ff))
* adjustments ([a3cfc6e](https://gitlab.com/diegoavieira/rdsystem/commit/a3cfc6e072df98b8aadea50d1734d777e60fd0ae))
* adjustments ([7853164](https://gitlab.com/diegoavieira/rdsystem/commit/78531641c7983211c1015b057309a4f49276a4f0))






## [0.0.25](https://gitlab.com/diegoavieira/rdsystem/compare/v0.0.24...v0.0.25) (2021-09-17)


### Bug Fixes

* adjustments ([77a5fca](https://gitlab.com/diegoavieira/rdsystem/commit/77a5fca1b764858e863d7f3fcb9e85ce0d74bbec))
* adjustments ([0e86cc9](https://gitlab.com/diegoavieira/rdsystem/commit/0e86cc979713a84c56129882689a00cf644bde64))






## [0.0.24](https://gitlab.com/diegoavieira/rdsystem/compare/v0.0.23...v0.0.24) (2021-09-16)


### Bug Fixes

* adjustments ([eece459](https://gitlab.com/diegoavieira/rdsystem/commit/eece459477e710618cdf02898e97c3eebb59972f))
* adjustments ([e006f0a](https://gitlab.com/diegoavieira/rdsystem/commit/e006f0af76b1d92455fc270fbc591fa146e3c5dd))






## [0.0.23](https://gitlab.com/diegoavieira/rdsystem/compare/v0.0.22...v0.0.23) (2021-09-16)


### Bug Fixes

* adjustments ([eb9d8e3](https://gitlab.com/diegoavieira/rdsystem/commit/eb9d8e368e6559d0d687d7fea755f4888ae923ee))





## [0.0.22](https://gitlab.com/diegoavieira/rdsystem/compare/v0.0.21...v0.0.22) (2021-09-16)


### Bug Fixes

* adjustments ([10347f6](https://gitlab.com/diegoavieira/rdsystem/commit/10347f6ced073e3b837d2b6129448c697496b596))
* adjustments ([37b413b](https://gitlab.com/diegoavieira/rdsystem/commit/37b413bd52967bc426718d0404771c2f30aa0f0f))
* adjustments ([635833e](https://gitlab.com/diegoavieira/rdsystem/commit/635833ece81996a03284cb3e090ba51aa7c1cab1))
* adjustments ([b8a3178](https://gitlab.com/diegoavieira/rdsystem/commit/b8a31789fbb4a7a9222ba7bb207f8accfc00c2ea))
* adjustments ([36b837a](https://gitlab.com/diegoavieira/rdsystem/commit/36b837adb9aabc60857c65e1b03f11b99a1f963b))
* adjustments ([f1de921](https://gitlab.com/diegoavieira/rdsystem/commit/f1de921587ca8486d83fe06a8ef69b297552edbb))
* adjustments ([73af481](https://gitlab.com/diegoavieira/rdsystem/commit/73af48184b7eee25cfdf7bfd0e3022e8e1cdb94f))
* adjustments ([93bc002](https://gitlab.com/diegoavieira/rdsystem/commit/93bc002026912cff7566d9d193f6ad73df577e4f))





## [0.0.21](https://gitlab.com/diegoavieira/rdsystem/compare/v0.0.20...v0.0.21) (2021-09-16)


### Bug Fixes

* adjustments ([337dcf8](https://gitlab.com/diegoavieira/rdsystem/commit/337dcf8867772617192fc9816237c29799fc9e13))






## [0.0.20](https://gitlab.com/diegoavieira/rdsystem/compare/v0.0.19...v0.0.20) (2021-09-16)


### Bug Fixes

* adjustments ([7d81dd3](https://gitlab.com/diegoavieira/rdsystem/commit/7d81dd3fa7ec27b79ec53c9601e7cac7ed687388))





## [0.0.19](https://gitlab.com/diegoavieira/rdsystem/compare/v0.0.18...v0.0.19) (2021-09-16)


### Bug Fixes

* adjustments ([889bf9d](https://gitlab.com/diegoavieira/rdsystem/commit/889bf9de836bc974469e550b4d691387b6358b42))





## [0.0.18](https://gitlab.com/diegoavieira/rdsystem/compare/v0.0.17...v0.0.18) (2021-09-16)


### Bug Fixes

* adjustments ([cf5b9b8](https://gitlab.com/diegoavieira/rdsystem/commit/cf5b9b80697f35e0b3a38574ec52af2893c5c4bc))





## [0.0.17](https://gitlab.com/diegoavieira/rdsystem/compare/v0.0.16...v0.0.17) (2021-09-16)


### Bug Fixes

* adjustments ([ad08021](https://gitlab.com/diegoavieira/rdsystem/commit/ad08021c1a6d72ef16c3f52f883d112c0003eb13))





## [0.0.16](https://gitlab.com/diegoavieira/rdsystem/compare/v0.0.15...v0.0.16) (2021-09-16)

**Note:** Version bump only for package rdsystem





## [0.0.15](https://gitlab.com/diegoavieira/rdsystem/compare/v0.0.14...v0.0.15) (2021-09-16)


### Bug Fixes

* adjustments ([526fab3](https://gitlab.com/diegoavieira/rdsystem/commit/526fab33f3e9d27b876c31c7f585f6a66dffd141))





## [0.0.14](https://gitlab.com/diegoavieira/rdsystem/compare/v0.0.13...v0.0.14) (2021-09-16)


### Bug Fixes

* adjustments ([f2464f3](https://gitlab.com/diegoavieira/rdsystem/commit/f2464f3c77e75e8ee91e5f41260237209f323967))
* adjustments ([9940c44](https://gitlab.com/diegoavieira/rdsystem/commit/9940c4469eef7ac48b16aa7926999e8341bf4059))
* adjustments ([e493026](https://gitlab.com/diegoavieira/rdsystem/commit/e493026df5f4b732065f90c5ef9c3838e2dcae7f))
* adjustments ([d9f3789](https://gitlab.com/diegoavieira/rdsystem/commit/d9f3789e1a5c026b6739924440bda03d5fdd1285))
* adjustments ([3915265](https://gitlab.com/diegoavieira/rdsystem/commit/391526548e570fd158dae822dfcc72af4d62ca5c))
* adjustments ([a215c36](https://gitlab.com/diegoavieira/rdsystem/commit/a215c36d7715e04d5c2672a94b50998469bab9f7))
* adjustments ([021057a](https://gitlab.com/diegoavieira/rdsystem/commit/021057ab006de6040ff9ad258e60f308ed70d5aa))
