#### Layout

The component layout is responsible for the application structure. It should contain the components RdsTheme, RdsContent, RdsHeader, RdsDrawer and RdsMain. It can be created according to the example:

{{"sandbox": "Layout.tsx", "frame": true}}

It can be imported in the App:

```tsx
const App = () => <Layout />;

export default App;
```
