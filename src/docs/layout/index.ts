import LayoutDemo from './Layout';
import LayoutCode from '!raw-loader!../../../src/docs/layout/Layout';

export default {
  'Layout.tsx': { demo: LayoutDemo, code: LayoutCode }
};
