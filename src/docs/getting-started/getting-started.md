#### Getting Started

**React Design System** contains components, hooks and other utilities. To be used, your project must contain the dependencies:

```json
"dependencies": {
  "@material-ui/core": "^4.12.2",
  "@material-ui/icons": "^4.11.2",
  "react": "^17.0.1",
  "react-dom": "^17.0.1"
}
```

##### Font and Icons

Apply the CDNs to the head of **public/index.html**.

```html
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" />
<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons" />
```

##### Components

The library **@rdsystem/common** contains the components. To install, run the npm command:

```sh
npm i @rdsystem/common
```

Access the **Components** menu to view them.

##### Layout

Access the **Layout** menu to view example to compose the layout.

##### Theme

Access the **Theme** menu to view example of theme customization.
